function share_history {
    history -a
    history -c
    history -r
}
PROMPT_COMMAND='share_history'
shopt -u histappend
HISTSIZE=50000
HISTTIMEFORMAT='%Y%m%d %T '

